#!/usr/bin/env bash
git clone https://gitlab.com/CommandCrafterHD/level-bot.git
cd level-bot
pip3 install -r requirements.txt
./run.sh
