# Import important stuff
import discord
import yaml
import sys
import sqlite3
import time
import random
import asyncio
import os
from shutil import copyfile

# Setting up some variables
client = discord.Client()  # Set the client
token = ""  # Set the token to "" (will get properly set in the load_settings function)
prefix = ""  # Set the prefix to "" (will get properly set in the load_settings function)
admins = ""  # Set the admins to "" (will get properly set in the load_settings function)
levels = []  # Set the levels to [] (will get properly set in the load_settings function)
timed_xp = {}  # Set the timed_xp to {} (will get properly set in the load_settings function)
message_xp = {}  # Set the message_xp to {} (will get properly set in the load_settings function)
scoreboard = {}  # Set the scoreboard to {} (will get properly set in the load_settings function)
levelup_channel = None  # Set the levelup_channel to None (will get properly set in the load_settings function)

# Setting up variables that wont change
conn = sqlite3.connect("files/lvl.db")  # Set the connection to be of type sqlite3.connect


class Colors:  # A nice list of colors to be used in Console (print commands)
    END = '\033[0m'  # Ends the ANSI escape sequence
    BOLD = '\033[1m'  # ANSI code for bold text
    UNDERLINED = '\033[4m'  # ANSI code for underlined text
    ERROR = '\033[31m'  # ANSI code for red text
    NOERROR = '\033[32m'  # ANSI code for green text
    INFO = '\33[36m'  # ANSI code for cyan text
    WARNING = '\033[33m'  # ANSI code for yellow text
    VALUE = '\033[34m'  # ANSI code for blue text


def load_settings():  # Loads in the settings
    global token, prefix, admins, timed_xp, message_xp, levels, scoreboard, levelup_channel
    try:
        config_file = open('config/config.yaml', 'r')  # Tries to open config/config.yaml
    except FileNotFoundError:  # If file doesnt exist, it tries to copy it
        print('[' + Colors.ERROR + 'ERROR' + Colors.END + '] File "' + Colors.VALUE + 'config/config.yaml' + Colors.END + '" does not exist! Copying it from "' + Colors.VALUE + 'files/config-example.yaml' + Colors.END + '" edit it and then start the bot again!')
        try:
            os.mkdir('config/')
            copyfile('files/config-example.yaml', 'config/config.yaml')
            sys.exit(1)
        except IOError:  # If it cant copy it, because of missing rights, it tells the user so
            print(
                '[' + Colors.ERROR + 'ERROR' + Colors.END + '] Coould not copy file from "' + Colors.VALUE + 'files/config-example.yaml' + Colors.END + '" to ' + Colors.VALUE + 'config/config.yaml' + Colors.END + 'try running the Bot as an administrator')
            sys.exit(1)

    config = yaml.load(config_file)  # Loads the yaml file as a dict
    token = str(config['token'])  # Sets the token
    prefix = str(config['prefix'])  # Sets the prefix
    admins = str(config['admins']).replace(' ', '').split(',')  # Sets the admin-list
    levels = config['levels']  # Sets the levels-list
    message_xp['active'] = bool(config['message_xp'])  # Sets the message_xp - active - info
    message_xp['cooldown'] = float(config['message_xp_cooldown'])  # Sets the message_xp - cooldown - info
    message_xp['min'] = int(config['message_xp_min'])  # Sets the message_xp - min - info
    message_xp['max'] = int(config['message_xp_max'])  # Sets the message_xp - max - info
    timed_xp['active'] = bool(config['timed_xp'])  # Sets the timed_xp - active - info
    timed_xp['value'] = int(config['timed_xp_value'])  # Sets the timed_xp - value - info
    timed_xp['timer'] = float(config['timed_xp_timer']) * 60  # Sets the timed_xp - timer - info
    scoreboard['active'] = bool(config['leaderboard'])
    scoreboard['channel_id'] = config['leaderboard_channel_id']
    scoreboard['message_id'] = config['leaderboard_message_id']
    levelup_channel = config['levelup_channel']

load_settings()


def get_top(amount):  # Gives out the top <amount> people in a nice list
    c = conn.cursor()
    c.execute("SELECT * FROM users ORDER BY xp DESC LIMIT ?;", (amount,))
    conn.commit()
    return c.fetchall()


async def update_scoreboard(members):  # Update the scoreboard to display the top X users
    em = discord.Embed(color=discord.Colour.blue(), title=":scroll:   __**SCOREBOARD**__ (Top 10)  :scroll: \n\n")
    index = 1
    member_list = ""
    level_list = ""
    xp_list = ""
    for i in members:
        member_list += str(index) + ". " + str(i[5]) + "\n"
        level_list += str(i[2]) + "\n"
        xp_list += str(i[1]) + "\n"
        index += 1
    em.add_field(name="Name", value=member_list, inline=True)
    em.add_field(name="Level", value=level_list, inline=True)
    em.add_field(name="Xp", value=xp_list, inline=True)
    await leaderboard_msg.edit(content="", embed=em)


def get_progress(now, end, old_level):
    if old_level is levels[len(levels) - 1]['xp_needed']:
        old_level = 0
    progress = 10 * (float(now-old_level)/float(end))
    progress_bar = "[" + ("=" * round(progress)) + ">" + (" " * (10-round(progress))) + "]"
    return [progress_bar, round(progress)]


def add_user(user):  # Adds a user to the database
    c = conn.cursor()
    c.execute("INSERT INTO Users (id, xp, lvl, lastmsg, pburl, name, discriminator) VALUES (?, ?, ?, ?, ?, ?, ?)", (int(user.id), 0, 0, str(time.time()), user.avatar_url, user.name, user.discriminator))
    conn.commit()
    print("A user has been added!")


def get_user(user):  # Gets User info
    c = conn.cursor()
    c.execute("SELECT * FROM Users WHERE id = ?", (user.id,))
    conn.commit()
    try:
        return c.fetchone()
    except:
        return None


async def check_for_levelup(user, message):  # Checks if a user has enough xp to level up and does so if he has
    apparent_user = get_user(user)
    try:
        if int(apparent_user[1]) > levels[int(apparent_user[2])]['xp_needed']:
            c = conn.cursor()
            c.execute("UPDATE Users SET lvl = ? WHERE id = ?", (apparent_user[2] + 1, user.id))
            conn.commit()
            if message is not None:
                if levels[apparent_user[2]]['role'] is not "None":
                    role = discord.utils.get(message.guild.roles, id=levels[apparent_user[2]]['role'])
                    await user.add_roles(role)
                await message.channel.send(content=":confetti_ball: <@" + str(apparent_user[0]) + "> You are now **Level " + str(apparent_user[2] + 1) + "**!", delete_after=20)
                if levelup_channel is not "None":
                    name = user.name
                    if user.nick is not None:
                        name = user.nick
                    em = discord.Embed(title=":arrow_up: Levelup", color=discord.Colour.blue(), description=name + " is now level `" + str(apparent_user[2] + 1) + "`")
                    _time = time.strftime("%d.%m.%Y %H:%M:%S", time.localtime())
                    em.set_footer(text=client.guilds[0].name + "  -  " + _time)
                    await client.get_channel(levelup_channel).send(embed=em)
    except:
        pass


async def give_xp(user, xp, message, auto):  # Function to give XP
    apparent_user = get_user(user)
    if apparent_user is None:
        add_user(user)
        await give_xp(user, xp, message, auto)
    else:
        if auto is True:
            c = conn.cursor()
            c.execute("UPDATE Users SET xp = ? WHERE id = ?", (apparent_user[1] + xp, user.id))
            conn.commit()
            await check_for_levelup(user, message)
        else:
            c = conn.cursor()
            c.execute("UPDATE Users SET xp = ?, lastmsg = ? WHERE id = ?", (apparent_user[1] + xp, str(time.time()), user.id))
            conn.commit()
            await check_for_levelup(user, message)
            await update_scoreboard(get_top(10))


async def add_time_xp():  # Adds X xp every Y minutes
    await client.wait_until_ready()
    while not client.is_closed():
        for memb in client.guilds[0].members:
            if not memb.bot:
                if memb.status.__str__() is not "offline":
                    await give_xp(memb, timed_xp['value'], None, True)
        await update_scoreboard(get_top(10))
        await asyncio.sleep(timed_xp['timer'])


@client.event
async def on_ready():  # Gives some info when the bot is ready
    print('Bot')
    print('------------------------------------')
    print('Name: ' + Colors.VALUE + client.user.name + Colors.END)
    print('ID: ' + Colors.VALUE + str(client.user.id) + Colors.END)
    print('------------------------------------')
    print('Config')
    print('------------------------------------')
    print('Prefix: ' + Colors.VALUE + prefix + Colors.END)
    print('Token: ' + Colors.VALUE + token + Colors.END)
    admin_list = "\n"
    for i in admins:
        admin_list += Colors.END + '- ' + Colors.VALUE + client.get_user(int(i)).name + Colors.END + '\n'
    print('Admins: ' + Colors.VALUE + admin_list[:-1] + Colors.END)
    print('------------------------------------')
    print('Message XP')
    print('------------------------------------')
    if message_xp['active']:
        print('Active: ' + Colors.NOERROR + 'True' + Colors.END)
    else:
        print('Active: ' + Colors.ERROR + 'False' + Colors.END)
    print('Cooldown: ' + Colors.VALUE + str(message_xp['cooldown']) + Colors.END + ' minutes')
    print('Minimum xp: ' + Colors.VALUE + str(message_xp['min']) + Colors.END)
    print('Maximum xp: ' + Colors.VALUE + str(message_xp['max']) + Colors.END)
    print('------------------------------------')
    print('Timed XP')
    print('------------------------------------')
    if timed_xp['active']:
        print('Active: ' + Colors.NOERROR + 'True' + Colors.END)
    else:
        print('Active: ' + Colors.ERROR + 'False' + Colors.END)
    print('Value: ' + Colors.VALUE + str(timed_xp['value']) + Colors.END)
    print('Timer: ' + Colors.VALUE + str(timed_xp['timer'] / 60) + Colors.END + ' minutes')
    print('------------------------------------')
    try:
        c = conn.cursor()
        c.execute("SELECT * FROM Users")
        conn.commit()
    except sqlite3.OperationalError:
        print("Database Users not found creating it!")
        c = conn.cursor()
        c.execute("CREATE TABLE Users (id int,xp int,lvl int,lastmsg float,pburl VARCHAR(255),name VARCHAR(20),discriminator int);")
        conn.commit()

    if scoreboard['active'] is True:
        ch = client.get_channel(scoreboard['channel_id'])
        if ch is None:
            print("ERROR! Scoreboard channel does not exist!!!")
            return
        else:
            if scoreboard['message_id'] is None:
                msg = await ch.send("This message will get replaced soon, its ID is: 0")
                await msg.edit(content="This message will get replaced soon, its ID is: " + str(msg.id))
        if scoreboard['message_id'] is not None:
            global leaderboard_msg
            leaderboard_msg = await ch.get_message(id=scoreboard['message_id'])
    if timed_xp['active'] is True:
        client.loop.create_task(add_time_xp())


@client.event
async def on_message(message):
    if message.author.id == client.user.id:  # Bail if the message was sent by the bot
        return

    if message.author.bot:  # Bail if the message author is a bot
        return

    if message_xp['active']:  # Give out message_xp
        apparent_user = get_user(message.author)  # Get the user
        if apparent_user is None:  # Create the user if non existant in database
            add_user(message.author)
            await give_xp(message.author, random.randrange(message_xp['min'], message_xp['max'] + 1), message, False)
            # await message.channel.send("Added user to database and gave xp!")  # This line is commented out because it is only to be used for debugging the bot
        elif time.time() - apparent_user[3] > message_xp['cooldown'] * 60:
            await give_xp(message.author, random.randrange(message_xp['min'], message_xp['max'] + 1), message, False)
            # await message.channel.send("Gave xp!")  # This line is commented out because it is only to be used for debugging the bot

    if message.content.startswith(prefix):
        if message.content.startswith(prefix + "help"):  # help command
            pass
        elif message.content.startswith(prefix + "editxp"):  # edit xp command
            if str(message.author.id) in admins:
                args = message.content.split(" ")
                if len(args) < 2:
                    await message.channel.send("Too few arguments! Format: {}editxp <USER-MENTION> (-)<XP>".format(prefix))
                elif len(message.mentions) > 0:
                    try:
                        await give_xp(message.mentions[0], int(args[2]), message, False)
                    except Exception as e:
                        await message.channel.send(str(e.with_traceback(e.__traceback__)))
                else:
                    await message.channel.send("Too few arguments! Format: {}editxp <USER-MENTION> (-)<XP>".format(prefix))
            else:
                await message.channel.send("You're not an admin!")
        elif message.content.startswith(prefix + "level"):  # level command
            apparent_user = get_user(message.author)  # Get the user
            if apparent_user is None:  # Create the user if non existant in database
                add_user(message.author)
                user = get_user(message.author)
                needed_xp = "You are already at the maximum level!"
                try:
                    if levels[int(apparent_user[2])]['xp_needed'] > 0:
                        needed_xp = get_progress(user[1], levels[int(user[2])]['xp_needed'], levels[int(user[2] - 1)]['xp_needed'])
                except IndexError:
                    pass
                em = discord.Embed(color=discord.Colour.blue(), description="You are level `" + str(user[2]) + "` with `" + str(user[1]) + "` xp!\n" + needed_xp[0] + " " + str(needed_xp[1] * 10) + "%\nYou are currently " + str(needed_xp[1] * 10) + "% (" + str(levels[int(user[2])]['xp_needed'] - user[1]) + "xp) away from reaching the next level!")
                await message.channel.send(embed=em, delete_after=20)
            else:
                user = apparent_user
                needed_xp = "You are already at the maximum level!"
                try:
                    if levels[int(apparent_user[2])]['xp_needed'] > 0:
                        needed_xp = get_progress(user[1], levels[int(user[2])]['xp_needed'], levels[int(user[2] - 1)]['xp_needed'])
                except IndexError:
                    pass
                em = discord.Embed(color=discord.Colour.blue(), description="You are level `" + str(user[2]) + "` with `" + str(user[1]) + "` xp!\n" + needed_xp[0] + " " + str(needed_xp[1] * 10) + "%\nYou are currently " + str(needed_xp[1] * 10) + "% (" + str(levels[int(user[2])]['xp_needed'] - user[1]) + "xp) away from reaching the next level!")
                await message.channel.send(embed=em, delete_after=20)

client.run(token)
